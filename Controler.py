# -*- encoding: utf-8 -*-

import socket
from subprocess import Popen
from Modele import *
from Vue import *
from Planet import *

class Controler():
    def __init__(self):
        # Class Callback
        self.classCallback ={
            'getObjetById': self.getObjetById,
            'getSelectedStats': self.getSelectedStats,
            'setPosition': self.setPosition,
            'getIpAddresse': self.getIpAddresse,
            'connectToServer': self.connectToServer,
            'partieSolo': self.partieSolo
        }
        self.listebutton=[["Attaque",self.attaque],["Test2",self.test2],["Test3",self.test3],["Test4",self.test4]]

        # Valeur pour le contoleur / Serveur:
        self.joueurCourant = Joueur(ip = self.getIpAddresse(), pseudo = "bob", couleur = "blue")
        #self.joueurCourant.ip = self.getIpAddresse()
        #self.ip =2 ################self.getIpAddresse()
        self.serveur = None
        self.cadre = 0
        self.actions = []
        self.attendre = 0

        # Initialisation de partie
        self.modele = Modele()
        self.egoserveur = 0

        # Creation de Joueurs
        #self.modele.createPlayer( "SuperMan", "blue" )

        # Dieu Créa l'univers en 7 Jours,
        # je l'ai créer en une ligne de code
        #self.modele.createPlayerUniverse()

        # Creation de la vue
        self.vue = Vue( self.listebutton, self.classCallback)
        self.vue.couleurJoueur = "blue"
        self.vue.createLayout(self.joueurCourant)
        #self.vue.creercadresplash(self.ip, self.modele.listeJoueur[0])


        # Print infos a propos des éléments
        for planet in self.modele.listePlanets:
            print("x:", planet.x, "y:", planet.y, "Rayon:", planet.rayon, "size:", planet.size,  "id:", planet.id, "tag:", planet.tag)
        for planet in self.modele.listeVaisseaux:
            print("x:", planet.x, "y:", planet.y, "size:", planet.size,  "id:", planet.id, "tag:", planet.tag)

        #print("Start Runner")
        #self.runner()

    def lancerpartie(self):
        # Dieu Créa l'univers en 7 Jours,
        # je l'ai créer en une ligne de code
        self.modele.createPlayerUniverse()
        self.modele.createUnit(self.modele.listePlanets[0], (75, 75), ('selectable','vaisseau', 'cargo'))
        self.modele.createUnit(self.modele.listePlanets[1], (158, 105), ('selectable','vaisseau', 'attaque'))
        self.vue.printElement(self.modele.listePlanets)
        self.vue.printElement(self.modele.listeVaisseaux)
        self.runner()

    def getIpAddresse(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # on cree un socket
        s.connect(("gmail.com",80))    # on envoie le ping
        monip=s.getsockname()[0] # on analyse la reponse qui contient l'IP en position 0
        s.close() # ferme le socket
        return monip

    def startServerProcess(self, ordinateur):
        if self.egoserveur == 0:
            if ordinateur == "ecole":
                pid = Popen(["C:\\Python34\\Python.exe", "./orion_mini_serveur.py"],shell=1).pid # on lance l'application serveur
                self.egoserveur=1
            elif ordinateur == "emilie":
                pass
            elif ordinateur == "miguel":
                pid = Popen(["/usr/bin/python", "./Server.py"], shell = 1).pid
                self.egoserveur=1
            #self.egoserveur=1 # on note que c'est soi qui, ayant demarre le serveur, aura le privilege de lancer la simulation

    def connectToServer(self):
        # L'inscription devrait se faire ailleur
        ipserveur=self.ip # lire le IP dans le champ du layout
        nom=self.listeJoueur[0] # noter notre nom
        if ipserveur and nom:
            ad="PYRO:controleurServeur@"+ipserveur+":9999" # construire la chaine de connection
            self.serveur = Pyro4.core.Proxy(ad) # se connecter au serveur
            self.monnom = nom
            rep=self.serveur.inscrireclient(self.monnom)    # on averti le serveur de nous inscrire
            #tester retour pour erreur de nom

    def lancerPartie(self, largeur, hauteur, mode):
        rep = self.serveur.lancerPartie(largeur, hauteur, mode)

    def partieSolo(self):
        print("holé")
        #self.vue.creeJeuMap()
        self.joueurCourant.setJoueur( self.vue.lobbyItems['pseudo'].get(), "blue" )
        self.modele.listeJoueur.append(self.joueurCourant)
        self.vue.changecadre(self.vue.boardFrame)
        self.lancerpartie()
        self.vue.root.update()

    def getObjetById(self, liste): # dla marde
        modeleObjet = []
        for n in liste:
            for v in self.modele.listeVaisseaux:
                if v.id in self.vue.spaceCanvas.gettags(n):
                    modeleObjet.append(v)
            for p in self.modele.listePlanets:
                if p.id in self.vue.spaceCanvas.gettags(n):
                    modeleObjet.append(v)
        return modeleObjet

    def getSelectedStats(self):
        for p in self.modele.listePlanets:
            if p.id in self.vue.spaceCanvas.gettags(self.vue.selected):
                return list(p.stats.items())
        for v in self.modele.listeVaisseaux:
            if v.id in self.vue.spaceCanvas.gettags(self.vue.selected):
                return list(v.stats.items())

    def setPosition(self, newPos):
        for e in self.vue.selected:
            print("e dans self.selected:", e)
            for v in self.modele.listeVaisseaux:
                print("v dans listeVaisseaux:", v.id)
                print("vaiseau ID:", v.id, "tag dans element:", self.vue.spaceCanvas.gettags(e))
                if v.id in self.vue.spaceCanvas.gettags(e):
                    print("True")
                    v.targetx = newPos[0]
                    v.targety = newPos[1]

    def moving(self):
        for v in self.modele.listeVaisseaux:
            if v.needToMove():
                delta = v.deplacer()
                self.vue.spaceCanvas.move(self.vue.spaceCanvas.find_withtag(v.id), delta[0], delta[1])

    def selectionEventListener(self): # Dla marde
        self.vue.drawSelection()
        if len(self.vue.selected) == 1:
            tmp = self.getObjetById(self.vue.selected)
            self.vue.showSelectionInfo(tmp[0].stats.copy())


    def runner(self):
        self.vue.drawSelection()
        #self.selectionEventListener()
        self.moving()
        self.vue.root.after(40, self.runner)

    #fonction de test qui sont appelees lorsqu'on pese sur un bouton
    def attaque(args):
        print("Attaque")
    def test2(args):
        print("test2")
    def test3(args):
        print("test3")
    def test4(args):
        print("test4")

if __name__ == '__main__':
    c = Controler()
    #c.runner()
    c.vue.root.mainloop()
    #c.vue.closeImages()
