# -*- encoding: utf-8 -*-
import math

class Vaisseau():
    def __init__(self, x, y, size, joueur):

        # Position
        self.x = x
        self.y = y
        self.size = size

        self.targetx = x
        self.targety = y
        # Propiété
        self.id = None
        self.image = None
        self.owner = joueur
        self.tag = None
        self.gas = None
        self.vitesse=3

        self.stats = {
            'propiétaires': joueur,
            'attaque' : 0,
            'disAttaque': 0,
            'chargement': 0,
            'vitesse': 3
        }

    def deplacementVaisseau(self,targetx,targety):
        print(self.x,self.y)

        if self.x < targetx:
            self.x = self.x + min(self.vitesse, targetx-self.x)
        else:
            self.x = self.x - min(self.vitesse, self.x-targetx)

        if self.y < targety:
            self.y = self.y + min(self.vitesse, targety-self.y)
        else:
            self.y = self.y - min(self.vitesse, self.y-targety)

    def needToMove(self):
        if self.x == self.targetx and self.y == self.targetx:
            return False
        else:
            return True

    def deplacer(self):
        #print(self.x,self.y)
        deltaX = 0
        deltaY = 0
        if self.x < self.targetx:
            deltaX =  min(self.vitesse, self.targetx - self.x)
            self.x += deltaX
        else:
            deltaX = min(self.vitesse, self.x - self.targetx) #* - 1
            deltaX *= -1
            self.x += deltaX
        if self.y < self.targety:
            deltaY = min(self.vitesse, self.targety - self.y)
            self.y += deltaY
        elif self.y > self.targety:
            deltaY = min(self.vitesse, self.y - self.targety) #* - 1
            deltaY *= -1
            self.y += deltaY
        #print(self.tag)
        #print("x :", self.x, "-> ", self.targetx, "delta:", deltaX)
        #print("y :", self.y, "-> ", self.targety, "delta:", deltaY)
        return (deltaX, deltaY)

    def checkBounding(self, maxXY):
        if self.x < size[0] / 2:
            self.x = size[0] / 2
        elif self.x > maxXY[0]:
            self.x = maxXY[0] - size[0] / 2
        if self.y < size[1] / 2:
            self.y = size[1] / 2
        elif self.y > maxXY[1]:
            self.y = maxXY[1] - size[1] / 2
